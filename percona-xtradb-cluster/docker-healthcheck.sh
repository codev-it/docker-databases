#!/bin/bash

# debug
if [ "${DEBUG}" = true ]; then
  set -eux
else
  set -e
fi

if [ "$(ps aux | pgrep -n mysqld)" != "" ]; then
  ping=$(mysqladmin ping --user=root --password="${MYSQL_ROOT_PASSWORD}" 2>/dev/null)
  if [[ "${ping}" == "mysqld is alive" ]]; then
    echo "success"
    exit 0
  fi
fi
echo "unhealthy"
exit 1
