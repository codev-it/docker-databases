# Docker Percona-Xtradb-Cluster

Docker Percona-Xtradb-Cluster fork with more functionality.

##### Currently supported discovery tools:

  * etcd
  * consul

##### Image tag info

Please note that the image with the tag "latest" uses the source code of the master branch.
We strongly recommend using only images with stability tags.

## Extensions

  * Include consul discovery service
  * Health-Check

## Config

#### Run Example

`docker run --name mysql -p 3306:3306 -e CLUSTER_NAME=cluster -e MYSQL_ROOT_PASSWORD=root codevit/percona-xtradb-cluster:stable`

#### Container Command

This image implements the full functionality off the main Percona-Xtradb-Cluster
Docker build.

The major different to the main build is the "DISCOVERY_SERVICE" env. value.
For the correct usage off the image, you ned to set the value like this.

  * ETCD -> etcd://etcd-host:port
  * Consul -> consul://consul-host:port
  
It is possible to pass the uri as entrypoint command.
The command has the higher priority compared to the env. variables.

#### Additional Env Vars

  * CRON_ENABLE               => Enable update cluster info by cron job (default: true)
  * CRON_TIMEOUT              => Set the cron job timout (default: 5s)

#### Documentation overview:

  * https://hub.docker.com/r/percona/percona-server/

#### Test Example

  * https://bitbucket.org/codev-it/docker-databases/src/master/test/