#!/bin/bash
#
# simply bash cron for docker container
#

set -e

# help command
if [[ ${1:-} == '-h' || ${1:-} == '--help' ]]; then
  echo "Usage: $0 <host> <log_file>"
  exit
fi

# cron info
CRON_ENABLE=${CRON_ENABLE:-false}
CRON_TIMEOUT=${CRON_TIMEOUT:-10s}
CRON_LAST_FULL=$(date +%s)
export CRON_LAST_FULL
CRON_LAST_INCREMENT=$(date +%s)
export CRON_LAST_INCREMENT
LAST_BASE_DIR=""
LAST_INC_DIR=""
INC_ID=1

# defaults
BACKUP_DIR=${1:-${BACKUP_DIR:-/var/backups}}
BACKUP_HOST=${1:-${BACKUP_HOST:-}}
BACKUP_PORT=${BACKUP_PORT:-}
BACKUP_USER=${BACKUP_USER:-}
BACKUP_PASSWORD=${BACKUP_PASSWORD:-}
BACKUP_FULL_TIMEOUT=${BACKUP_FULL_TIMEOUT:-'7 days'}
BACKUP_INCREMENT_TIMEOUT=${BACKUP_INCREMENT_TIMEOUT:-'1 days'}
INCLUDE_SQL_BACKUP=${INCLUDE_SQL_BACKUP:-false}

# build args
function _args() {
  local args=""
  if [[ -n "${BACKUP_HOST}" ]]; then
    args="${args} --host=${BACKUP_HOST}"
  fi
  if [[ -n "${BACKUP_PORT}" ]]; then
    args="${args} --port=${BACKUP_PORT}"
  fi
  if [[ -n "${BACKUP_USER}" ]]; then
    args="${args} --user=${BACKUP_USER}"
  fi
  if [[ -n "${BACKUP_PASSWORD}" ]]; then
    args="${args} --password=${BACKUP_PASSWORD}"
  fi
  echo "${args}"
}

# innobackupex command short cut
function _backup() {
  local cmd=$*
  args=$(_args)
  args=${args//--user=${BACKUP_USER}/--user=xtrabackup}
  args=${args//--password=${BACKUP_PASSWORD}/--password=${XTRABACKUP_PASSWORD}}
  eval "innobackupex --no-timestamp \
                     --ftwrl-wait-threshold=40 \
                     --ftwrl-wait-query-type=all \
                     --ftwrl-wait-timeout=180 \
                     --kill-long-queries-timeout=20 \
                     --kill-long-query-type=all ${cmd} ${args} || true"
}

# mysqldump command short cut
function _backup_sql() {
  local dir=${1:-}
  if [[ ${INCLUDE_SQL_BACKUP} == true ]]; then
    args=$(_args)
    test -d "${dir}" || mkdir -p "${dir}"
    databases=$(eval "mysql ${args} -N -e 'SHOW DATABASES;' 2>/dev/null")
    for database in ${databases}; do
      if [[ ${database} != "sys" ]] &&
        [[ ${database} != "mysql" ]] &&
        [[ ${database} != "information_schema" ]] &&
        [[ ${database} != "performance_schema" ]]; then
        _backup_sql_msg "${database}"
        eval "mysqldump ${args} --opt \
        --skip-add-locks \
        --complete-insert \
        --single-transaction \
        ${database} > ${dir}/${database}.sql 2>/dev/null || true"
      fi
    done
  fi
}

function _backup_sql_msg() {
  echo "########################################"
  echo "Backup SQL Database ${1:-}"
  echo "########################################"
}

function _backup_successful() {
  echo "########################################"
  echo "Backup successful!"
  echo "########################################"
}

# check if time for exec
function check_cron() {
  local current=$(($(date +%s) + 1))
  last_env=$(printenv "${1:-}")
  last=$(date -d @"${last_env}")
  next=$(date +%s -d "${last} + ${2:-}")
  if [[ "${current}" -ge "$(date +%s -d "${last}")" ]] &&
    [[ "${current}" -ge "${next}" ]]; then
    echo "true"
  elif [[ "${current}" -ge "$(date +%s -d "${last}")" ]] &&
    [[ "${current}" -le "$(($(date +%s -d "${last}") + 1))" ]]; then
    echo "true"
  else
    echo "false"
  fi
}

# remove backups order then
function remove_old_backups() {
  current=$(date)
  old=$(date +%s -d "${current} - ${BACKUP_REMOVE_ORDER_THEN:-12 days}")
  if [[ ${old} != "" ]]; then
    for dir in "${BACKUP_DIR}"/*; do
      if [[ -d ${dir} ]] &&
        [[ ${dir} != "${BACKUP_DIR}/*" ]]; then
        dir_time=$(echo "${dir}" | cut -d'_' -f3)
        if [[ ${dir_time} -lt ${old} ]]; then
          rm -R "${dir}"
        fi
      fi
    done
  fi
}

# run cron controller
RUN_INC=true
if [[ "${CRON_ENABLE}" == "true" ]]; then
  sleep 5m
  while true; do
    if [[ "$(/healthcheck)" == "success" ]]; then
      run_full_backup=$(check_cron CRON_LAST_FULL "${BACKUP_FULL_TIMEOUT}")
      run_inc_backup=$(check_cron CRON_LAST_INCREMENT "${BACKUP_INCREMENT_TIMEOUT}")
      if [[ "${run_full_backup}" == "true" ]]; then
        echo "########################################"
        echo "Run full backup | $(date)"
        echo "########################################"
        LAST_BASE_DIR="${BACKUP_DIR}/$(date +%F_%R_%s)_full/raw"
        _backup "${LAST_BASE_DIR}"
        _backup_sql "${LAST_BASE_DIR//raw/sql/}"
        CRON_LAST_FULL=$(date +%s)
        INC_ID=1
        _backup_successful
        RUN_INC=false
      fi

      if [[ "${RUN_INC}" == "true" ]] &&
        [[ "${run_full_backup}" != "true" ]] &&
        [[ "${run_inc_backup}" == "true" ]] &&
        [[ ${CRON_LAST_FULL} -ne ${CRON_LAST_INCREMENT} ]]; then
        echo "########################################"
        echo "Run increment backup ${INC_ID} | $(date)"
        echo "########################################"
        INC_DIR="${BACKUP_DIR}/$(date +%F_%R_%s)_inc/raw"
        if [[ ${INC_ID} == 1 ]]; then
          _backup --incremental "${INC_DIR}" --incremental-basedir="${LAST_BASE_DIR}"
        else
          _backup --incremental "${INC_DIR}" --incremental-basedir="${LAST_INC_DIR}"
        fi
        _backup_sql "${INC_DIR//raw/sql/}"
        LAST_INC_DIR="${INC_DIR}"
        CRON_LAST_INCREMENT=$(date +%s)
        INC_ID=$((INC_ID + 1))
        _backup_successful
      fi
      RUN_INC=true
    fi
    remove_old_backups
    sleep "${CRON_TIMEOUT}"
  done
fi
