#!/bin/bash

set -e

# help command
if [[ ${1:-} == '-h' || ${1:-} == '--help' ]]; then
  echo "Usage: $0 <host> <user> <password> [OPTS]"
  exit
fi

# defaults
CMD=$*
DIR=${BACKUP_DIR:-/var/backups}

# format command arguments value from arguments
function fmt_arg_val() {
  local name=${1:-}
  local env=${2:-}
  env_val=$(printenv "${env}" || echo "")
  if ! echo "${CMD}" | grep -q "${name}" && [[ -n "${env_val}" ]]; then
    CMD="${CMD} --${name}=${env_val}"
  fi
}

# get last dir
function dir_by_time() {
  local time=${1:-}
  local dir_s=""
  local ret=""
  for dir in "${DIR}"/*; do
    if [[ -d "${dir}" ]]; then
      dir_time=$(echo "${dir}" | cut -d'_' -f3)
      dir_s="${dir_time},${dir_s}"
    fi
  done
  if [[ -n "${dir_s}" ]]; then
    IFS=',' read -ra ARR <<<"${dir_s%?}"
    sorted=$(printf "%s\n" "${ARR[@]}" | sort -n | tr '\r\n' ',')
    IFS=',' read -ra SORTED <<<"${sorted[@]}"
    if [[ "${time}" == "" ]]; then
      ret="${SORTED[-1]}"
    else
      timestamp=$(echo "${time}" | grep -o -e '[0-9]*')
      pattern=${time/${timestamp}/}
      for s in "${SORTED[@]}"; do
        case $pattern in
        \<) if [[ ${s} -lt ${timestamp} ]]; then
          ret="${s}"
          break
        fi ;;
        \>) if [[ ${s} -gt ${timestamp} ]]; then
          ret="${s}"
          break
        fi ;;
        *) if [[ ${s} -eq ${timestamp} ]]; then
          ret="${s}"
          break
        fi ;;
        esac
      done
    fi
    for dir in "${DIR}"/*; do
      if [[ ${dir} =~ ${ret} ]]; then
        echo "${dir}"
      fi
    done
  fi
}

# format the default command
fmt_arg_val "host" "BACKUP_HOST"
fmt_arg_val "port" "BACKUP_PORT"
fmt_arg_val "user" "BACKUP_USER"
fmt_arg_val "password" "BACKUP_PASSWORD"

# remofe args
if [[ ${1} == "sql" ]] || [[ ${1} == "raw" ]]; then
  CMD=${CMD//${1}/}
fi

# get backup data by time
if [[ $(echo "${CMD}" | grep -o '\-\-time=') == "" ]]; then
  CURR_BACKUP_DIR=$(dir_by_time "")
else
  TIME=$(echo "${CMD}" | grep -o -e 'time=["<>0-9]*' | cut -d'=' -f2)
  CURR_BACKUP_DIR=$(dir_by_time "${TIME}")
fi

# prepare and restore
if [[ -d "${CURR_BACKUP_DIR}" ]]; then
  case ${1} in
  raw)
    args=$(echo "${CMD}" | sed -E 's/--time=["<>0-9]*//g')
    mysqld stop || true
    rm -R /var/lib/mysql/*
    xtrabackup --prepare --target-dir="${CURR_BACKUP_DIR}/raw"
    eval "xtrabackup  --copy-back --rsync \
      --force-non-empty-directories \
      --target-dir=${CURR_BACKUP_DIR}/raw ${args}"
    mysqld start || true
    ;;
  *)
    args=$(echo "${CMD}" | sed -E 's/--time=["<>0-9]*//g')
    args=$(echo "${args}" | sed -E 's/--database=[-\._a-zA-Z0-9]*//g')
    db_name=$(echo "${CMD}" | grep -o -e 'database=[-\._a-zA-Z0-9]*' | cut -d'=' -f2)
    if [[ -n ${db_name} ]]; then
      if [[ -f ${CURR_BACKUP_DIR}/sql/${db_name}.sql ]]; then
        eval "mysql ${args} -e 'DROP DATABASE IF EXISTS ${db_name};' 2>/dev/null"
        eval "mysql ${args} -e 'CREATE DATABASE IF NOT EXISTS ${db_name};' 2>/dev/null"
        eval "mysql ${args} ${db_name} < ${CURR_BACKUP_DIR}/sql/${db_name}.sql 2>/dev/null"
      fi
    else
      databases=$(eval "mysql ${args} -N -e 'SHOW DATABASES;' 2>/dev/null")
      for database in ${databases}; do
        if [[ ${database} != "sys" ]] &&
          [[ ${database} != "mysql" ]] &&
          [[ ${database} != "information_schema" ]] &&
          [[ ${database} != "performance_schema" ]]; then
          eval "mysql ${args} -e 'DROP DATABASE IF EXISTS ${database};' 2>/dev/null"
        fi
      done
      for file in "${CURR_BACKUP_DIR}"/sql/*; do
        db_name=$(echo "${file##*/}" | cut -d'.' -f1)
        eval "mysql ${args} -e 'CREATE DATABASE IF NOT EXISTS ${db_name};' 2>/dev/null"
        eval "mysql ${args} ${db_name} < ${file} 2>/dev/null"
      done
    fi
    ;;
  esac
fi
