# Builder
FROM alpine:3.10 as builder
MAINTAINER Codev-IT <office@codev-it.at>

# install depends
WORKDIR /tmp
RUN set -eux \
  && mkdir -p /src/usr/bin \
  && apk add -t build-depends \
    git

# copy docker scripts
COPY docker-entrypoint.sh /src/entrypoint
COPY docker-healthcheck.sh /src/healthcheck

# copy custom bin scripts
COPY bin/backup-cron.sh /src/usr/bin/backup-cron
COPY bin/restore.sh /src/usr/bin/restore

# prepare script permissions
RUN set -eux \
  && chmod +x /src/entrypoint \
  && chmod +x /src/healthcheck \
  && chmod +x /src/usr/bin/backup-cron \
  && chmod +x /src/usr/bin/restore

# Final
FROM codevit/percona-xtradb-cluster:latest
MAINTAINER Codev-IT <office@codev-it.at>

# env
ARG UID=988
ARG GID=988
ENV REPLICATE=true \
    BACKUP_PORT=3306 \
    BACKUP_USER=root

# set workdir
WORKDIR /var/backups

# set user to build
USER root

# copy build files
COPY --from=builder /src /

# install xtrabackup
RUN set -eux \
  && yum update -y \
  && yum install -y \
    jq \
  && yum install percona-xtrabackup-24 \
  && usermod -u ${UID} mysql \
  && groupmod -g ${GID} mysql \
  && find / -user 1001 -exec chown -h mysql {} \; || true \
  && find / -group 1001 -exec chgrp -h mysql {} \; || true  \
  && chown mysql:mysql /var/backups \
  && rm -rf /var/cache/yum \
  && rm -rf /var/backups/* \
  && rm -rf /tmp/*

# set workung user
USER ${UID}

# volumes
VOLUME ["/var/lib/mysql", "/var/log/mysql", "/var/backups"]

# docker entrypoint and commands
ENTRYPOINT ["/entrypoint"]
CMD ["--backup-cron --bootstrap"]

# Build-time metadata as defined at http://label-schema.org
ARG BUILD_DATE
ARG VCS_REF
ARG VERSION
LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.name="percona-xtradb-master" \
      org.label-schema.description="Docker Percona-Xtradb-Cluster with Xtrabackup." \
      org.label-schema.url="https://www.codev-it.at" \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url="https://bitbucket.org/codev-it/docker-databases.git" \
      org.label-schema.vendor="Codev-IT" \
      org.label-schema.version=$VERSION \
      org.label-schema.schema-version="1.0"