# Docker Percona-Xtradb-Master

Use this image as a bootstrap node, and join other node on it.

##### Image tag info

Please note that the image with the tag "latest" uses the source code of the master branch.
We strongly recommend using only images with stability tags.

## Extensions

  * Health check
  + Xtrabackup

## Config

#### Run Example

**Run the xtrabackup command**

`docker run --rm -it -v /tmp/mysql:/var/lib/mysql/ -v /tmp/backup:/var/backups codevit/xtrabackup:stable --backup --host= --user=root --password=test`

**Run as instance and make backups by cron**

`docker run --rm -it -v /tmp/mysql:/var/lib/mysql/ -v /tmp/backup:/var/backups codevit/xtrabackup:stable --backup-cron`

#### Container Command

The default entrypoint is the xtrabackup command, the available arguments can be
found in the default documentation.

  * https://www.percona.com/doc/percona-xtrabackup/2.4/index.html
  
**Cron Config**

To create backups via cron jobs use the "--backup-cron" option. This
option create a complete backup regularly and an incremental in small intervals 
from the last full backup.
The "FULL_BACKUP_TIMEOUT" and "INCREMENT_BACKUP_TIMEOUT" env. vars set the
Timeouts between the backups.

**Additional Scripts**

For a better use of the additional scripts, we recommend to use the env. vars.

`restore sql|raw --host=*|BACKUP_HOST --port=*|BACKUP_PORT --user=*|BACKUP_USER --password=*|BACKUP_PASSWORD [--database=*] [--time=*]`

To restore the Backups can u use the restore script, this script restore the
last backups.
The "--database" option can be used to restore a specific database.
The "--time" option specifies the time stamp of the backups to be used.
If not specified, the last backup is used and is automatically copied.
With "<", ">" can be specified more exactly.
e.g. --time=">1550344278".

!! DANGER !!
The raw restore function is very danger, use of your own risk.

#### Env Vars

  * DISCOVERY_SERVICE         => Discovery service uri: service://host:port
    * ETCD -> etcd://etcd-host:port
    * Consul -> consul://consul-host:port
  * CLUSTER_NAME              => Name of the cluster to be backed up
  * CLUSTER_JOIN              => Join an exist cluster by ip or domain, leave empty by using discovery services.
  * REPLICATE                 => Create an replicate, disable if only use the xtrabackup command.
  * MYSQL_ROOT_PASSWORD       => Is needed for the Replication.
  * XTRABACKUP_PASSWORD       => Password for the xtrabackup user. (*required)
  * BACKUP_HOST               => The mysql host, do not set if you use the discovery service [--host].
  * BACKUP_PORT               => The mysql port [--port].
  * BACKUP_USER               => The mysql backup user [--user]. (default: root)
  * BACKUP_PASSWORD           => The mysql backup user password [--password].
  * BACKUP_FULL_TIMEOUT       => Set the full backup timout. (default: 7 days)
  * BACKUP_INCREMENT_TIMEOUT  => Set the increment backup timeout. (default: 1 days)
  * BACKUP_REMOVE_ORDER_THEN  => Set the increment backup timeout. (default: 12 days)
  * INCLUDE_SQL_BACKUP        => Include separate SQL files for all Databases. (default: true)

TIME OPTIONS SYNTAX: [UNIT] [TIME]
months, days, hours, minutes, seconds

  * 1 days
  * 2 hours
  * 30 minutes

#### Test Example

  * https://bitbucket.org/codev-it/docker-databases/src/master/test/