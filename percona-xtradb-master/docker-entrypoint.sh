#!/bin/bash

# debug
if [ "${DEBUG}" = true ]; then
  set -eux
else
  set -e
fi

# defaults
CMD=$*
CRON_ENABLE=${CRON_ENABLE:-false}
WAIT_BEFORE_START=${WAIT_BEFORE_START:-0}

# set env. vars permanently
function _export() {
  eval "export ${1:-}=${2:-}"
}

# format command arguments value from arguments
function fmt_arg_val() {
  local name=${1:-}
  local env=${2:-}
  env_val=$(printenv "${env}" || echo "")
  if ! echo "${CMD}" | grep -q "${name}" && [[ -n "${env_val}" ]]; then
    CMD="${CMD} --${name}=${env_val}"
  else
    for opt in "${CMD[@]}"; do
      val=$(echo "${opt}" | cut -d'=' -f2)
      if [[ "${opt}" =~ ${name} ]] && [[ -n "${val}" ]]; then
        _export "${env}" "${val/ /}"
      fi
    done
  fi
}

# wait for start timer
if [[ -n ${WAIT_BEFORE_START} ]]; then
  echo >&2 "Info:  Wait ${WAIT_BEFORE_START} befor start"
  sleep "${WAIT_BEFORE_START}"
  _export WAIT_BEFORE_START 0
fi

# check discovery service env. var
if [[ "${DISCOVERY_SERVICE}" ]] && [[ "${CLUSTER_NAME}" ]]; then
  host_ip=$(hostname -i | awk '{print $1}')
  ips=$(discovery-service "${DISCOVERY_SERVICE}" "${CLUSTER_NAME}" ip-list self)
  if ! echo "${CMD}" | grep -q host && [[ "${ips[0]}" ]]; then
    _export BACKUP_HOST "${ips[0]}"
    CMD="${CMD} --host=${ips[0]}"
  fi
fi

# format the default command
fmt_arg_val "target-dir" "BACKUP_DIR"
fmt_arg_val "host" "BACKUP_HOST"
fmt_arg_val "port" "BACKUP_PORT"
fmt_arg_val "user" "BACKUP_USER"
fmt_arg_val "password" "BACKUP_PASSWORD"

# set cron true by cli cmd
if echo "${CMD}" | grep -q backup-cron; then
  _export CRON_ENABLE true
  _export REPLICATE true
fi

# set cron true by cli cmd
if [[ ${BOOTSTRAP:-true} == true ]]; then
  rm /var/lib/mysql/galera.cache || true
  rm /var/lib/mysql/grastate.dat || true
fi

# default controller
if [[ "${CRON_ENABLE}" == 'true' ]]; then
  backup-cron &
  bash /entrypoint.sh mysqld
else
  eval "xtrabackup ${CMD}"
fi
