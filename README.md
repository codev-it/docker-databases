# Docker Databases

Docker database image collection.
To create database cluster systems in conjunction with Discovery Services or standalone.

_All images are swarm compatible._

##### Currently supported discovery tools:

  * etcd
  * consul

##### Image Tag Info

Please note that the image with the tag "latest" uses the source code of the master branch.
We strongly recommend using only images with stability tags.

## Docker Images Overview

  * [Proxy-SQL](https://bitbucket.org/codev-it/docker-databases/src/master/proxysql/)
   => Proxy for SQL connections.
  * [Percona Xtradb Cluster](https://bitbucket.org/codev-it/docker-databases/src/master/percona-xtradb-cluster/)
   => Percona Xtradb Cluster Server
  * [MySQL](https://bitbucket.org/codev-it/docker-databases/src/master/mysql/)
   => MySQL Galera Server
  * [MariaDB](https://bitbucket.org/codev-it/docker-databases/src/master/mariadb/)
   => MariaDB Galera Server
  * [Xtrabackup](https://bitbucket.org/codev-it/docker-databases/src/master/xtrabackup/)
   => Percona-Xtrabackup service.