#!/bin/bash

# debug
if [ "${DEBUG}" = true ]; then
  set -eux
else
  set -e
fi

# helpers
_gotpl() {
  if [ -f "/etc/gotpl/$1" ]; then
    gotpl "/etc/gotpl/$1" >"$2"
  fi
}

# set mysql root user if not exist
export MYSQL_ROOT_USER=${MYSQL_ROOT_USER:-'root'}

# check discovery service env. var
export DISCOVERY_SERVICE=${1:-${DISCOVERY_SERVICE}}
if [[ -z "${DISCOVERY_SERVICE:-${CLUSTER_JOIN}}" ]]; then
  echo >&2 'Error:  You need to specify DISCOVERY_SERVICE'
  exit 1
fi

# check cluster name env. var
if [[ -z "${CLUSTER_NAME}" ]]; then
  echo >&2 'Error:  You need to specify CLUSTER_NAME'
  exit 1
fi

# check mysql root password env. var
if [[ -z "${MYSQL_ROOT_PASSWORD}" ]]; then
  echo >&2 'Error:  You need to specify MYSQL_ROOT_PASSWORD'
  exit 1
fi

# write config file
_gotpl proxysql.cnf.tmpl /etc/proxysql.cnf

# run cron in background
docker-cron /var/log/cron.log &

# start proxy sql
su - proxy -c "proxysql --initial -f -c /etc/proxysql.cnf"
