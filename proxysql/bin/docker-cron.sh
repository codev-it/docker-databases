#!/bin/bash
#
# simply bash cron for docker container
#

# help command
if [[ $1 == '-h' || $1 == '--help' ]]; then
  echo "Usage: $0 <log_file>"
  exit
fi

# default params
ERR_FILE="${1:-/dev/null}"

# cron info
CRON_ENABLE=${CRON_ENABLE:-true}
CRON_TIMEOUT=${CRON_TIMEOUT:-10s}

# run cron controller
if [[ "${CRON_ENABLE}" == 'true' ]]; then
  while true; do
    if [[ "$(/healthcheck)" == "success" ]]; then
      clustercheck 2>>"${ERR_FILE}" >>"${ERR_FILE}"
    fi
    sleep "${CRON_TIMEOUT}"
  done
fi
