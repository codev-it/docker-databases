#!/bin/bash

set -e

# redeclare env vars
DISCOVERY_SERVICE=${DISCOVERY_SERVICE:-}
CLUSTER_JOIN=${CLUSTER_JOIN:-}
CLUSTER_NAME=${CLUSTER_NAME:-}
MYSQL_ROOT_USER=${MYSQL_ROOT_USER:-root}
MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD:-}
MYSQL_PROXY_USER=${MYSQL_PROXY_USER:-${MYSQL_ROOT_USER}}
MYSQL_PROXY_PASSWORD=${MYSQL_PROXY_PASSWORD:-${MYSQL_ROOT_PASSWORD}}

# global vars
UPDATE=false
LOCALHOST=127.0.0.1
SERVICE_URL=${DISCOVERY_SERVICE##*/}
CLUSTER_NAME=${CLUSTER_NAME%%:*}

# mysql command poly
function _mysql() {
  eval "mysql -u${MYSQL_ROOT_USER} -p${MYSQL_ROOT_PASSWORD} $*"
}

# mysql proxy command poly
function _proxy() {
  eval "mysql -h ${LOCALHOST} -P6032 -uproxysql -pproxysql $*"
}

# mysql proxy command poly
function _discovery_service_mysql() {
  eval "discovery-service ${DISCOVERY_SERVICE} ${CLUSTER_NAME} $*"
}

# get active cluster nodes
function cluster_get_nodes() {
  nodes=$(_proxy "-e 'SELECT hostname FROM mysql_servers;'" | tr '\n' ',')
  nodes=${nodes/hostname,/}
  if [[ -n "${nodes%?}" ]]; then
    echo "${nodes%?}"
  fi
}

# add new cluster node
function cluster_add_node() {
  echo "------------ ADD NODE: ${1} ------------"
  ping=$(mysqladmin ping -h "${1}" --user="${MYSQL_ROOT_USER}" --password="${MYSQL_ROOT_PASSWORD}" 2>/dev/null)
  if [[ "${ping}" == "mysqld is alive" ]]; then
    if [[ ${MYSQL_PROXY_USER} != root ]]; then
      for ip in $(hostname -i); do
        _mysql -h "${1}" "-e 'GRANT ALL ON *.* TO \"${MYSQL_PROXY_USER}\"@\"${ip}\" IDENTIFIED BY \"${MYSQL_PROXY_PASSWORD}\";'"
      done
    fi
    _proxy "-e 'INSERT INTO mysql_servers (hostgroup_id, hostname, port, max_replication_lag) VALUES (0, \"${1}\", 3306, 20);'"
    UPDATE=true
  fi
}

# remove cluster node
function cluster_remove_node() {
  echo "------------ REMOVE NODE: ${1} ------------"
  _proxy "-e 'DELETE FROM mysql_servers WHERE hostname = \"${1}\";'"
  UPDATE=true
}

# default cluster setting
function cluster_add_user() {
  local user="${MYSQL_PROXY_USER}"
  local pass="${MYSQL_PROXY_PASSWORD}"
  # set mysql user if not exist
  response=$(_proxy "-e 'SELECT count(username) FROM mysql_users WHERE username=\"${user}\";'")
  user_exist=$(tr " " '\n' <<<"$response" | tr '\n' " " | cut -d ' ' -f2)
  if [[ ${user_exist:-0} -eq 0 ]]; then
    _proxy "-e 'INSERT INTO mysql_users (username, password, active, default_hostgroup, max_connections) VALUES (\"${user}\", \"${pass}\", 1, 0, 200);'"
  fi
}

# default cluster setting
function cluster_check_update() {
  if [[ ${UPDATE} == true ]]; then
    _proxy "-e 'LOAD MYSQL SERVERS TO RUNTIME; SAVE MYSQL SERVERS TO DISK; LOAD MYSQL USERS TO RUNTIME; SAVE MYSQL USERS TO DISK;'"
  fi
}

# check cluster nodes
function cluster_check_nodes() {
  local cluster_nodes=${1:-}
  exists_nodes=$(cluster_get_nodes)
  IFS=',' read -ra ADDR <<<"${exists_nodes}"
  for ip in "${ADDR[@]}"; do
    if [[ $(echo "${cluster_nodes}" | grep -o -e "${ip}") == "" ]]; then
      cluster_remove_node "${ip}"
    fi
  done
  IFS=',' read -ra ADDA <<<"${cluster_nodes}"
  for ip in "${ADDA[@]}"; do
    if [[ $(echo "${exists_nodes}" | grep -o -e "${ip}") == "" ]]; then
      cluster_add_node "${ip}"
    fi
  done
}

# default
function cluster_check_default() {
  cluster_check_nodes "${CLUSTER_JOIN}"
}

# discovery
function cluster_check_discovery() {
  ip_list=$(_discovery_service_mysql ip-list)
  cluster_check_nodes "${ip_list}"
}

# start cluster add check function
echo
echo 'Run cluster check'
echo
if [[ -n "${SERVICE_URL}" ]] && [[ -n "${CLUSTER_NAME}" ]]; then
  cluster_add_user
  cluster_check_discovery
  cluster_check_update
elif [[ -n "${CLUSTER_JOIN}" ]]; then
  cluster_add_user
  cluster_check_default
  cluster_check_update
fi
