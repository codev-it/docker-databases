#!/bin/bash

set -eux

# redeclare env vars
MYSQL_ROOT_USER=${MYSQL_ROOT_USER:-root}
MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD:-}

# global vars
UPDATE=false
LOCALHOST=127.0.0.1

# mysql command poly
function _mysql() {
  eval "mysql -u${MYSQL_ROOT_USER} -p${MYSQL_ROOT_PASSWORD} $*"
}

# mysql proxy command poly
function _proxy() {
  eval "mysql -h ${LOCALHOST} -P6032 -uproxysql -pproxysql $*"
}

# get active cluster nodes
function cluster_get_nodes() {
  nodes=$(_proxy "-e 'SELECT hostname FROM mysql_servers;'" | tr '\n' ',')
  nodes=${nodes/hostname,/}
  if [[ -n "${nodes%?}" ]]; then
    echo "${nodes%?}"
  fi
}

# default cluster setting
function cluster_update() {
  if [[ ${UPDATE} == true ]]; then
    _proxy "-e 'LOAD MYSQL SERVERS TO RUNTIME; SAVE MYSQL SERVERS TO DISK; LOAD MYSQL USERS TO RUNTIME; SAVE MYSQL USERS TO DISK;'"
  fi
}

# add new user
function user_add() {
  local user="${1:-}"
  local pass="${2:-}"
  # set mysql user if not exist
  response=$(_proxy "-e 'SELECT count(username) FROM mysql_users WHERE username=\"${user}\";'")
  user_exist=$(tr " " '\n' <<<"$response" | tr '\n' " " | cut -d ' ' -f2)
  if [[ ${user_exist:-0} -eq 0 ]]; then
    _proxy "-e 'INSERT INTO mysql_users (username, password, active, default_hostgroup, max_connections) VALUES (\"${user}\", \"${pass}\", 1, 0, 200);'"
  fi

  exists_nodes=$(cluster_get_nodes)
  IFS=',' read -ra ADDA <<<"${exists_nodes}"
  for node in "${ADDA[@]}"; do
    for ip in $(hostname -i); do
      _mysql -h "${node}" "-e 'GRANT ALL ON *.* TO \"${user}\"@\"${ip}\" IDENTIFIED BY \"${pass}\";'"
    done
  done
  UPDATE=true
}

# add new user
function user_rm() {
  local user="${1:-}"
  _proxy "-e 'DELETE FROM mysql_users WHERE username=\"${user}\";'"
  exists_nodes=$(cluster_get_nodes)
  IFS=',' read -ra ADDA <<<"${exists_nodes}"
  for node in "${ADDA[@]}"; do
    for ip in $(hostname -i); do
      _mysql -h "${node}" "-e 'DROP USER IF EXISTS ${user}@${ip};'"
    done
  done
  UPDATE=true
}

# start cluster add check function
case ${1} in
user-add) user_add "${2}" "${3}" ;;
user-rm) user_rm "${2}" ;;
esac
cluster_update
