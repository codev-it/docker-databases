# Docker ProxySQL

Proxy-SQL Alpine Linux with discovery service support.

##### Currently supported discovery tools:

  * etcd
  * consul

##### Image tag info

Please note that the image with the tag "latest" uses the source code of the master branch.
We strongly recommend using only images with stability tags.

## Extensions

  * Discovery service support (etcd, consul, ...)
  * Health check

## Config

#### Run Example

`docker run --name proxysql -p 6033:6033 -p 6032:6032 -e CLUSTER_NAME=cluster -e MYSQL_ROOT_PASSWORD=root codevit/proxysql:stable consul://localhost:8500`

#### Container Command

It is possible to pass the uri as entrypoint command.
The command has the higher priority compared to the env. variables.

**Additional Scripts**

For a better use of the additional scripts, we recommend to use the env. vars.

`proxysql-admin user-add [NAME] [PW]`
`proxysql-admin user-rm [NAME]`

#### Env Vars

  * DISCOVERY_SERVICE         => Discovery service uri: service://host:port
    * ETCD -> etcd://etcd-host:port
    * Consul -> consul://consul-host:port
  * DISCOVERY_SERVICE_SUBNET  => Set used ip range for docker, regex possible
  * CLUSTER_NAME              => Database Cluster name and discovery service key (*required)
  * CLUSTER_JOIN              => IP's addresses of the SQL hosts, if no discovery
  service is available, the host can be specified directly (multiple separate with ',')
  * MYSQL_ROOT_USER           => The mysql root user (default: root)
  * MYSQL_ROOT_PASSWORD       => The mysql root user password (*required)
  * MYSQL_PROXY_USER          => The proxy user name (default: MYSQL_ROOT_USER)
  * MYSQL_PROXY_PASSWORD      => The proxy user password (default: MYSQL_ROOT_PASSWORD)
  * CRON_ENABLE               => Enable update cluster info by cron job (default: true)
  * CRON_TIMEOUT              => Set the cron job timout (default: 10s)
  * CONSUL_HTTP_TOKEN         => Consul ACL HTTP token for secure connections

#### Test Example

  * https://bitbucket.org/codev-it/docker-databases/src/master/test/