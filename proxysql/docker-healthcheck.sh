#!/bin/bash

# debug
if [ "${DEBUG}" = true ]; then
  set -eux
else
  set -e
fi

if [ "$(ps aux | pgrep -n proxysql)" != "" ]; then
  ping=$(mysqladmin ping -h 127.0.0.1 -P6032 --user="${MONITOR_USERNAME}" --password="${MONITOR_PASSWORD}" 2>/dev/null)
  if [[ "${ping}" == "mysqld is alive" ]]; then
    echo "success"
    exit 0
  fi
fi
echo "unhealthy"
exit 1
